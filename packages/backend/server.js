const express = require('express');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let db = new sqlite3.Database('./db/TTDB.db', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the database.');
});

app.get('/', (req, res) => {
  res.send('Running');
});

// ----------------------FOODTRUCK-------------------
//-----------------------GET-------------------------
//get all foodtrucks
app.get('/foodtruck', (req, res) => {
  let sql = 'SELECT * FROM FoodTruck';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get foodtruck by ID
app.get('/foodtruck/:id', (req, res) => {
  let sql = 'SELECT * FROM FoodTruck where ID = ?';
  let params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'FoodTruck not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get foodtrucks by OwnerID
app.all('/foodtruck/o/:id', (req, res) => {
  let sql = 'SELECT * FROM FoodTruck where OwnerID = ?';
  let params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new foodtruck
app.post('/foodtruck', (req, res) => {
  let errors = [];
  if (!req.body.plate) {
    errors.push('No plate specified');
  }
  if (!req.body.make) {
    errors.push('No make specified');
  }
  if (!req.body.model) {
    errors.push('No Model specified');
  }
  if (!req.body.ownerId) {
    errors.push('No Owner specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  let data = {
    plate: req.body.plate,
    make: req.body.make,
    inUse: req.body.inUse,
    model: req.body.model,
    alias: req.body.alias,
    ownerId: req.body.ownerId,
  };
  let sql =
    'INSERT INTO foodtruck (plate, Make, inUse, model, alias, ownerId) VALUES (?,?,?,?,?,?)';
  let params = [
    data.plate,
    data.make,
    data.inUse,
    data.model,
    data.alias,
    data.ownerId,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update foodtruck
app.patch('/foodtruck/:id', (req, res) => {
  let data = {
    plate: req.body.plate,
    make: req.body.make,
    inUse: req.body.inUse,
    model: req.body.model,
    alias: req.body.alias,
    ownerId: req.body.ownerId,
  };
  let sql = `UPDATE foodtruck set 
    plate = COALESCE(?,plate),
    make = COALESCE(?,make),
    inUse = COALESCE(?,inUse),
    model = COALESCE(?,model),
    alias = COALESCE(?,alias),
    ownerId = COALESCE(?,ownerId)
    where id = ?`;
  let params = [
    data.plate,
    data.make,
    data.inUse,
    data.model,
    data.alias,
    data.ownerId,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/foodtruck/:id', (req, res) => {
  let sql = 'DELETE FROM FoodTruck where ID = ?';
  let params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'deleted',
      changes: result.changes,
    });
  });
});

// ----------------------USER-------------------
//-----------------------GET-------------------------
//get all users
app.get('/user', (req, res) => {
  let sql = 'SELECT * FROM user';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get user by ID
app.get('/user/:id', (req, res) => {
  let sql = 'SELECT * FROM user where ID = ?';
  let params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'user not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get users by OwnerID
app.all('/user/o/:id', (req, res) => {
  let sql = 'SELECT * FROM user where OwnerID = ?';
  let params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new user
app.post('/user', (req, res) => {
  let errors = [];
  if (!req.body.login) {
    errors.push('No login specified');
  }
  if (!req.body.password) {
    errors.push('No password specified');
  }
  if (!req.body.name) {
    errors.push('No name specified');
  }
  if (!req.body.surname) {
    errors.push('No surname specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  let data = {
    login: req.body.login,
    password: req.body.password,
    isOwner: req.body.isOwner,
    isManager: req.body.isManager,
    name: req.body.name,
    surname: req.body.surname,
    ownerId: req.body.ownerId,
  };
  let sql =
    'INSERT INTO user (login, password, isOwner, isManager, name, surname, ownerId) VALUES (?,?,?,?,?,?,?)';
  let params = [
    data.login,
    data.password,
    data.isOwner,
    data.isManager,
    data.name,
    data.surname,
    data.ownerId,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update user
app.patch('/user/:id', (req, res) => {
  let data = {
    login: req.body.login,
    password: req.body.password,
    isOwner: req.body.isOwner,
    isManager: req.body.isManager,
    name: req.body.name,
    surname: req.body.surname,
    ownerId: req.body.ownerId,
  };
  let sql = `UPDATE user set 
    login = COALESCE(?,login),
    password = COALESCE(?,password),
    isOwner = COALESCE(?,isOwner),
    isManager = COALESCE(?,isManager),
    name = COALESCE(?,name),
    surname = COALESCE(?,surname),
    ownerId = COALESCE(?,ownerId)
    where id = ?`;
  let params = [
    data.login,
    data.password,
    data.isOwner,
    data.isManager,
    data.name,
    data.surname,
    data.ownerId,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    if (result && result.changes > 0) {
      res.json({
        message: 'deleted',
        changes: result.changes,
      });
    } else {
      res.status(404).json({
        message: 'No record found to delete',
        changes: 0,
      });
    }
  });
});
//-----------------------DELETE------------------------
app.delete('/user/:id', (req, res) => {
  let sql = 'DELETE FROM user where ID = ?';
  let params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: res.message });
    }
    res.json({
      message: 'deleted',
      changes: result.changes,
    });
  });
});

// ----------------------PRODUCT-------------------
//-----------------------GET-------------------------
//get all products
app.get('/product', (req, res) => {
  const sql = 'SELECT * FROM product';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get product by ID
app.get('/product/:id', (req, res) => {
  const sql = 'SELECT * FROM product where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'product not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get product by StockID
app.all('/product/s/:id', (req, res) => {
  const sql = 'SELECT * FROM product where StockID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new product
app.post('/product', (req, res) => {
  const errors = [];
  if (!req.body.name) {
    errors.push('No name specified');
  }
  if (!req.body.unit) {
    errors.push('No unit specified');
  }
  if (!req.body.inStock) {
    errors.push('No inStock specified');
  }
  if (!req.body.stockId) {
    errors.push('No stockId specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    name: req.body.name,
    unit: req.body.unit,
    inStock: req.body.inStock,
    maxStock: req.body.maxStock,
    notify: req.body.notify,
    notifyWhenBelow: req.body.notifyWhenBelow,
    stockId: req.body.stockId,
  };
  const sql =
    'INSERT INTO product (name, unit, inStock, maxStock, notify, notifyWhenBelow, stockId) VALUES (?,?,?,?,?,?,?)';
  const params = [
    data.name,
    data.unit,
    data.inStock,
    data.maxStock,
    data.notify,
    data.notifyWhenBelow,
    data.stockId,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update product
app.patch('/product/:id', (req, res) => {
  const data = {
    name: req.body.name,
    unit: req.body.unit,
    inStock: req.body.inStock,
    maxStock: req.body.maxStock,
    notify: req.body.notify,
    notifyWhenBelow: req.body.notifyWhenBelow,
    stockId: req.body.stockId,
  };
  const sql = `UPDATE product set 
    name = COALESCE(?,name),
    unit = COALESCE(?,unit),
    inStock = COALESCE(?,inStock),
    maxStock = COALESCE(?,maxStock),
    notify = COALESCE(?,notify),
    notifyWhenBelow = COALESCE(?,notifyWhenBelow),
    stockId = COALESCE(?,stockId)
    WHERE id = ?`;
  const params = [
    data.name,
    data.unit,
    data.inStock,
    data.maxStock,
    data.notify,
    data.notifyWhenBelow,
    data.stockId,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/product/:id', (req, res) => {
  const sql = 'DELETE FROM product where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
      changes: result.changes,
    });
  });
});

// ----------------------STOCK-------------------
//-----------------------GET-------------------------
//get all stocks
app.get('/stock', (req, res) => {
  const sql = 'SELECT * FROM stock';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get stock by ID
app.get('/stock/:id', (req, res) => {
  const sql = 'SELECT * FROM stock where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'stock not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get stock by ownerID
app.all('/stock/o/:id', (req, res) => {
  const sql = 'SELECT * FROM stock where OwnerID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get stock by foodtruckID
app.all('/stock/f/:id', (req, res) => {
  const sql = 'SELECT * FROM stock where foodtruckID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new stock
app.post('/stock', (req, res) => {
  const errors = [];
  if (!req.body.ownerID) {
    errors.push('No owner specified');
  }
  if (!req.body.foodtruckID) {
    errors.push('No foodtruck specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    ownerID: req.body.ownerID,
    foodtruckID: req.body.foodtruckID,
  };
  const sql = 'INSERT INTO stock (ownerID, foodtruckID) VALUES (?,?)';
  const params = [data.ownerID, data.foodtruckID];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update stock
app.patch('/stock/:id', (req, res) => {
  const data = {
    ownerID: req.body.ownerID,
    foodtruckId: req.body.foodtruckID,
  };
  const sql = `UPDATE stock set 
    ownerID = COALESCE(?,ownerID),
    foodtruckID = COALESCE(?,ownerID)
    WHERE id = ?`;
  const params = [data.ownerID, data.foodtruckID, req.params.id];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/stock/:id', (req, res) => {
  const sql = 'DELETE FROM stock where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
    });
  });
});

// ----------------------CHAT-------------------
//-----------------------GET-------------------------
//get all chats
app.get('/chat', (req, res) => {
  const sql = 'SELECT * FROM chat';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get chat by ID
app.get('/chat/:id', (req, res) => {
  const sql = 'SELECT * FROM chat where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'chat not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get chat by users
app.get('/chat/:u1/:u2', (req, res) => {
  const sql =
    'SELECT * FROM chat WHERE (user1 = ? OR user1 = ?) AND (user2 = ? OR user2 = ?)';
  const params = [req.params.u1, req.params.u2, req.params.u1, req.params.u2];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new chat
app.post('/chat', (req, res) => {
  const errors = [];
  if (!req.body.user1) {
    errors.push('No user1 specified');
  }
  if (!req.body.user2) {
    errors.push('No user2 specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    user1: req.body.user1,
    user2: req.body.user2,
  };
  const sql = 'INSERT INTO chat (user1, user2) VALUES (?,?)';
  const params = [data.user1, data.user2];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update chat
app.patch('/chat/:id', (req, res) => {
  const data = {
    user1: req.body.user1,
    user2: req.body.user2,
  };
  const sql = `UPDATE chat set 
    user1 = COALESCE(?,user1),
    user2 = COALESCE(?,user2)
    WHERE id = ?`;
  const params = [data.user1, data.user1, req.params.id];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/chat/:id', (req, res) => {
  const sql = 'DELETE FROM chat where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
    });
  });
});

// ----------------------MESSAGE-------------------
//-----------------------GET-------------------------
//get all messages
app.get('/message', (req, res) => {
  const sql = 'SELECT * FROM message';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get message by ID
app.get('/message/:id', (req, res) => {
  const sql = 'SELECT * FROM message where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'message not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get message by users
app.all('/message/c/:id', (req, res) => {
  const sql = 'SELECT * FROM message WHERE chatID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new message
app.post('/message', (req, res) => {
  const errors = [];
  if (!req.body.text) {
    errors.push('No text specified');
  }
  if (!req.body.senderID) {
    errors.push('No sender specified');
  }
  if (!req.body.timeStamp) {
    errors.push('No time specified');
  }
  if (!req.body.chatID) {
    errors.push('No chat specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    text: req.body.text,
    senderID: req.body.senderID,
    timeStamp: req.body.timeStamp,
    chatID: req.body.chatID,
  };
  const sql =
    'INSERT INTO message (text, senderID, timeStamp, chatID) VALUES (?,?,?,?)';
  const params = [data.text, data.senderID, data.timeStamp, data.chatID];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update message
app.patch('/message/:id', (req, res) => {
  const data = {
    text: req.body.text,
    senderID: req.body.senderID,
    timeStamp: req.body.timeStamp,
    chatID: req.body.chatID,
  };
  const sql = `UPDATE message set 
    text = COALESCE(?,text),
    senderID = COALESCE(?,senderID),
    timeStamp = COALESCE(?,timeStamp),
    chatID = COALESCE(?,chatID)
    WHERE id = ?`;
  const params = [
    data.text,
    data.senderID,
    data.timeStamp,
    data.chatID,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/message/:id', (req, res) => {
  const sql = 'DELETE FROM message where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
    });
  });
});

// ----------------------FOODTRUCKTIMETABLE-------------------
//-----------------------GET-------------------------
//get all foodTruckTimetables
app.get('/ftTime', (req, res) => {
  const sql = 'SELECT * FROM foodTruckTimetable';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get foodTruckTimetable by ID
app.get('/ftTime/:id', (req, res) => {
  const sql = 'SELECT * FROM foodTruckTimetable where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'foodTruckTimetable not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get foodTruckTimetable by foodtruck
app.all('/ftTime/f/:id', (req, res) => {
  const sql = 'SELECT * FROM foodTruckTimetable WHERE FoodtruckID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new foodTruckTimetable
app.post('/ftTime', (req, res) => {
  const errors = [];
  if (!req.body.FoodtruckID) {
    errors.push('No Foodtruck specified');
  }
  if (!req.body.DateT) {
    errors.push('No date');
  }
  if (!req.body.TimeStart) {
    errors.push('No start time specified');
  }
  if (!req.body.TimeEnd) {
    errors.push('No end time specified');
  }
  if (!req.body.Address) {
    errors.push('No adress specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    FoodtruckID: req.body.FoodtruckID,
    DateT: req.body.DateT,
    TimeStart: req.body.TimeStart,
    TimeEnd: req.body.TimeEnd,
    Address: req.body.Address,
    Note: req.body.Note,
  };
  const sql =
    'INSERT INTO foodTruckTimetable (FoodtruckID, DateT, TimeStart, TimeEnd, Address, Note) VALUES (?,?,?,?,?,?)';
  const params = [
    data.FoodtruckID,
    data.DateT,
    data.TimeStart,
    data.TimeEnd,
    data.Address,
    data.Note,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update foodTruckTimetable
app.patch('/ftTime/:id', (req, res) => {
  const data = {
    FoodtruckID: req.body.FoodtruckID,
    DateT: req.body.DateT,
    TimeStart: req.body.TimeStart,
    TimeEnd: req.body.TimeEnd,
    Address: req.body.Address,
    Note: req.body.Note,
  };
  const sql = `UPDATE foodTruckTimetable set 
    FoodtruckID = COALESCE(?,FoodtruckID),
    DateT = COALESCE(?,DateT),
    TimeStart = COALESCE(?,TimeStart),
    TimeEnd = COALESCE(?,TimeEnd),
    Address = COALESCE(?,Address),
    Note = COALESCE(?,Note)
    WHERE id = ?`;
  const params = [
    data.FoodtruckID,
    data.DateT,
    data.TimeStart,
    data.TimeEnd,
    data.Address,
    data.Note,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/ftTime/:id', (req, res) => {
  const sql = 'DELETE FROM foodTruckTimetable where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
    });
  });
});

// ----------------------WORKERTIMETABLE-------------------
//-----------------------GET-------------------------
//get all workerTimetables
app.get('/wTime', (req, res) => {
  const sql = 'SELECT * FROM workerTimetable';
  db.all(sql, [], (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get workerTimetable by ID
app.get('/wTime/:id', (req, res) => {
  const sql = 'SELECT * FROM workerTimetable where ID = ?';
  const params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ message: 'workerTimetable not found' });
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

//get workerTimetable by foodtruck
app.all('/wTime/f/:id', (req, res) => {
  const sql = 'SELECT * FROM workerTimetable WHERE FoodtruckID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//get workerTimetable by foodtruck
app.all('/wTime/w/:id', (req, res) => {
  const sql = 'SELECT * FROM workerTimetable WHERE WorkerID = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

//-----------------------POST------------------------
//create new workerTimetable
app.post('/wTime', (req, res) => {
  const errors = [];
  if (!req.body.WorkerID) {
    errors.push('No worker specified');
  }
  if (!req.body.FoodtruckID) {
    errors.push('No Foodtruck specified');
  }
  if (!req.body.DateW) {
    errors.push('No date');
  }
  if (!req.body.TimeStart) {
    errors.push('No start time specified');
  }
  if (!req.body.TimeEnd) {
    errors.push('No end time specified');
  }
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  const data = {
    FoodtruckID: req.body.FoodtruckID,
    DateW: req.body.DateW,
    TimeStart: req.body.TimeStart,
    TimeEnd: req.body.TimeEnd,
    WorkerID: req.body.WorkerID,
  };
  const sql =
    'INSERT INTO workerTimetable (FoodtruckID, DateW, TimeStart, TimeEnd, WorkerID) VALUES (?,?,?,?,?)';
  const params = [
    data.FoodtruckID,
    data.DateW,
    data.TimeStart,
    data.TimeEnd,
    data.WorkerID,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

//-----------------------PATCH------------------------
//update workerTimetable
app.patch('/wTime/:id', (req, res) => {
  const data = {
    FoodtruckID: req.body.FoodtruckID,
    DateW: req.body.DateW,
    TimeStart: req.body.TimeStart,
    TimeEnd: req.body.TimeEnd,
    WorkerID: req.body.WorkerID,
  };
  const sql = `UPDATE workerTimetable set 
    FoodtruckID = COALESCE(?,FoodtruckID),
    DateW = COALESCE(?,DateW),
    TimeStart = COALESCE(?,TimeStart),
    TimeEnd = COALESCE(?,TimeEnd),
    WorkerID = COALESCE(?,WorkerID)
    WHERE id = ?`;
  const params = [
    data.FoodtruckID,
    data.DateW,
    data.TimeStart,
    data.TimeEnd,
    data.WorkerID,
    req.params.id,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.changes,
    });
  });
});
//-----------------------DELETE------------------------
app.delete('/wTime/:id', (req, res) => {
  const sql = 'DELETE FROM workerTimetable where ID = ?';
  const params = [req.params.id];
  db.run(sql, params, (err, result) => {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.json({
      message: 'delete',
    });
  });
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
