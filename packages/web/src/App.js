import React from 'react';

const App = () => {
  return (
    <div style={styles.container}>
      <h1 style={styles.header}>Hello, World!</h1>
    </div>
  );
};

// Tutaj używamy obiektów JavaScript do stylizacji, które są podobne do CSS
const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh', // 100% wysokości widoku
  },
  header: {
    fontSize: '2rem',
    fontWeight: 'bold',
  },
};

export default App;