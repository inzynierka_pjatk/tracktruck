# TrackTruck App

## Aplikacja webowa/mobilna wspierająca właścicieli Food Trucków w efektywnym zarządzaniu biznesem

### Opis Projektu

TrackTruck to innowacyjna aplikacja webowa dedykowana właścicielom sieci Food Trucków. Nasz projekt ma na celu umożliwienie efektywnego zarządzania wszystkimi aspektami działalności, od kontroli nad zatrudnionymi pracownikami, przez monitorowanie kosztów, zarządzanie zapasami, aż po analizowanie dochodów. Nasza platforma została zaprojektowana, aby umożliwić zdalne gromadzenie niezbędnych informacji od personelu poprzez łatwą w obsłudze aplikację.

### Cele Projektu

- Opracowanie aplikacji internetowej dla właścicieli Food Trucków.
- Usprawnienie kontroli i zarządzania wszystkimi aspektami działalności biznesowej.
- Zdalne gromadzenie i analizowanie danych biznesowych.

### Rezultaty Projektu

- W pełni funkcjonalna aplikacja webowa spełniająca założone funkcjonalności.

### Miary Sukcesu

- Zrealizowanie projektu do dnia 17 czerwca 2024.
- Wytworzenie oprogramowania działającego zgodnie z oczekiwaniami.
- Implementacja projektu w wybranych technologiach.

### Ograniczenia

- Ograniczony czas z powodu obowiązków zawodowych i innych zobowiązań.
- Mały zespół składający się z trzech osób.
- Praca nad projektem poza regularnymi godzinami pracy.
- Brak budżetu na oprogramowanie i technologie płatne.

### Zespół Projektowy

- Adam Lichy (S20162) - Aplikacje Internetowe, tryb niestacjonarny
- Szymon Ciemny (S21355) - Aplikacje Internetowe, tryb niestacjonarny
- Marcin Dymek (S15661) - Aplikacje Internetowe, tryb niestacjonarny
- Michał Cichowski (S20695) - Aplikacje Internetowe, tryb niestacjonarny

### Promotor

- dr hab. Bartosz Marcinkowski

### Data Ukończenia Projektu

- Koniec semestru letniego

---

## Instalacja i Uruchomienie

(Tutaj możesz dodać instrukcje dotyczące instalacji i uruchomienia aplikacji)

## Współpraca

(Zachęcamy do współpracy nad projektem. Tutaj możesz dodać informacje o tym, jak można się do niego przyczynić.)

## Licencja

(Tutaj możesz określić rodzaj licencji, na jakiej udostępniasz projekt.)

## Kontakt

(Tutaj możesz podać informacje kontaktowe dla osób zainteresowanych projektem.)
